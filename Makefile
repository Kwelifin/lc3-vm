TEST_SRC := $(filter-out src/main.c, $(wildcard src/*.c)) tests/tests.c

all: lc3.out

lc3.out:
	mkdir -p bin
	gcc src/*.c -o bin/lc3.out -I inc --std=c11

memcheck: lc3.out
	valgrind --tool=memcheck bin/lc3.out

test:
	gcc $(TEST_SRC) -I inc -o bin/test_run.app --std=c11
	bin/test_run.app
