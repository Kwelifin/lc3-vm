#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

// Unix specific
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/termios.h>
#include <sys/mman.h>
// end

#include "lc3internal.h"
#include "lc3.h"

#define ENCODE_PARAM(instruction, offset, bitcount) (((instruction) >> (offset)) & ((1 << (bitcount)) - 1))

lc3_ptr new_lc3()
{
  lc3_ptr state_pointer = (lc3_ptr)malloc(sizeof(lc3_state));
  lc3_state* lc3 = (lc3_state*) state_pointer;
  lc3->running = false;
  for(int i = 0; i < R_COUNT; i++)
  {
    lc3->reg[i] = 0;
  }
  return state_pointer;
}

void destroy_lc3(lc3_ptr lc3)
{
  free(lc3);
}

uint16_t check_key()
{
    fd_set readfds;
    FD_ZERO(&readfds);
    FD_SET(STDIN_FILENO, &readfds);

    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 0;
    return select(1, &readfds, NULL, NULL, &timeout) != 0;
}

uint16_t sign_extend(uint16_t x, int bit_count)
{
  if ((x >> (bit_count-1)) & 0b1)
  {
    x |= (0xFFFF << bit_count);
  }
  return x;
}
void update_cond_flags(lc3_state* lc3, uint16_t value)
{
  if (value == 0)
  {
    lc3->reg[R_COND] = COND_FL_ZRO;
  }
  else if (value >> 15)
  {
    lc3->reg[R_COND] = COND_FL_NEG;
  }
  else
  {
    lc3->reg[R_COND] = COND_FL_POS;
  }
}

void mem_write(lc3_state* lc3, uint16_t address, uint16_t value)
{
  lc3->memory[address] = value;
}

uint16_t mem_read(lc3_state* lc3, uint16_t address)
{
  if (address == MR_KBSR)
  {
    if (check_key())
    {
      lc3->memory[MR_KBSR] = (1 << 15);
      lc3->memory[MR_KBDR] = getchar();
    }
    else 
    {
      lc3->memory[MR_KBSR] = 0;
    }
  }
  return lc3->memory[address];
}

/* ADD instruction encoding
 *
 *  15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
 * +--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--+
 * |    0001   |   DR   |   SR1  |0 | 0 0 |   SR2  |
 * +--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--+
 * 
 *  15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
 * +--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--+
 * |    0001   |   DR   |   SR1  |1 |     imm5     |
 * +--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--+
 *
 * If bit [5] is 0, the second source operand is obtained from SR2. 
 * If bit [5] is 1, thesecond source operand is obtained by sign-extending the imm5 field to 16 bits.
 * In both cases, the second source operand is added to the contents of SR1 and the result stored in DR. 
 * The condition codes are set, based on whether the result is negative, zero, or positive.
 *
 * Examples
 * ADD R2, R3, R4 ; R2 <- R3 + R4
 * ADD R2, R3, #7 ; R2 <- R3 + 7
 */
void process_add(lc3_state* lc3, uint16_t instr)
{
  uint16_t dr = ENCODE_PARAM(instr, 9, 3);
  uint16_t sr1 = ENCODE_PARAM(instr, 6, 3);
  bool sr2_is_literal = ENCODE_PARAM(instr, 5, 1);
  uint16_t result;
  if (sr2_is_literal)
  {
    uint16_t imm5 = ENCODE_PARAM(instr, 0, 5);
    result = lc3->reg[sr1] + sign_extend(imm5, 5);
  }
  else
  {
    uint16_t sr2 = ENCODE_PARAM(instr, 0, 3);
    result = lc3->reg[sr1] + lc3->reg[sr2];
  }
  lc3->reg[dr] = result;
  update_cond_flags(lc3, result);
}

/* Bitwise AND instruction encoding
 *
 *  15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
 * +--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--+
 * |    0101   |   DR   |   SR1  |0 | 0 0 |   SR2  |
 * +--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--+
 * 
 *  15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
 * +--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--+
 * |    0101   |   DR   |   SR1  |1 |     imm5     |
 * +--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--+
 *
 * If bit [5] is 0, the second source operand is obtained from SR2.
 * If bit [5] is 1,the second source operand is obtained by sign-extending the imm5 field to 16bits.
 * In either case, the second source operand and the contents of SR1 are bit-wise ANDed, 
 * and the result stored in DR. The condition codes are set, based on whether the binary 
 * value produced, taken as a 2’s complement integer, is negative,zero, or positive.
 *
 * Examples
 * AND R2, R3, R4 ; R2 <- R3 & R4
 * AND R2, R3, #7 ; R2 <- R3 & 7
 */
void process_and(lc3_state* lc3, uint16_t instr)
{
  uint16_t dest = ENCODE_PARAM(instr, 9, 3);
  uint16_t sr1 = ENCODE_PARAM(instr, 6, 3);
  bool sr2_is_literal = ENCODE_PARAM(instr, 5, 1);
  uint16_t result;
  if (sr2_is_literal)
  {
    uint16_t imm5 = ENCODE_PARAM(instr, 0, 5);
    result = lc3->reg[sr1] & sign_extend(imm5, 5);
  }
  else
  {
    uint16_t sr2 = ENCODE_PARAM(instr, 0, 3);
    result = lc3->reg[sr1] & lc3->reg[sr2];
  }
  lc3->reg[dest] = result;
  update_cond_flags(lc3, result);
}

/* Bitwise Complement (NOT) instruction encoding
 *
 *  15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
 * +--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--+
 * |    1001   |   DR   |   SR   |1 |    11111     |
 * +--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--+
 *
 * The bit-wise complement of the contents of SR is stored in DR. The condition
 * codes are set, based on whether the binary value produced, taken as a 2’s
 * complement integer, is negative, zero, or positive.
 *
 * Examples
 * NOT R4, R2 ; R4 <- NOT(R2)
 */
void process_not(lc3_state* lc3, uint16_t instr)
{
  uint16_t dest = ENCODE_PARAM(instr, 9, 3);
  uint16_t sr = ENCODE_PARAM(instr, 6, 3);
  uint16_t result = ~(lc3->reg[sr]);
  lc3->reg[dest] = result;
  update_cond_flags(lc3, result);
}

void process_br(lc3_state* lc3, uint16_t instr)
{
  uint16_t rcond = lc3->reg[R_COND];
  uint16_t expected = ENCODE_PARAM(instr, 9, 3);
  if (rcond & expected)
  {
    uint16_t pc_offset9 = ENCODE_PARAM(instr, 0, 9);
    lc3->reg[R_PC] += sign_extend(pc_offset9, 9);
  }
}

void process_jmp(lc3_state* lc3, uint16_t instr)
{
  uint16_t base_r = ENCODE_PARAM(instr, 6, 3);
  lc3->reg[R_PC] = lc3->reg[base_r];
}

void process_jsr(lc3_state* lc3, uint16_t instr)
{
  uint16_t is_jsr = ENCODE_PARAM(instr, 11, 1);
  lc3->reg[R_R7] = lc3->reg[R_PC];
  if (is_jsr) // JSR
  {
    uint16_t pc_offset11 = ENCODE_PARAM(instr, 0, 11);
    lc3->reg[R_PC] += sign_extend(pc_offset11, 11);
  }
  else // JSRR
  {
    uint16_t base_r = ENCODE_PARAM(instr, 6, 3);
    lc3->reg[R_PC] = lc3->reg[base_r];
  }
}

void process_ld(lc3_state* lc3, uint16_t instr)
{
  uint16_t dr = ENCODE_PARAM(instr, 9, 3);
  uint16_t pc_offset9 = ENCODE_PARAM(instr, 0, 9);
  uint16_t address = lc3->reg[R_PC] + sign_extend(pc_offset9, 9);
  lc3->reg[dr] = mem_read(lc3, address);
  update_cond_flags(lc3, lc3->reg[dr]);
}

void process_ldi(lc3_state* lc3, uint16_t instr)
{
  uint16_t dr = ENCODE_PARAM(instr, 9, 3);
  uint16_t pc_offset9 = ENCODE_PARAM(instr, 0, 9);
  uint16_t address = lc3->reg[R_PC] + sign_extend(pc_offset9, 9);
  lc3->reg[dr] = mem_read(lc3, mem_read(lc3, address));
  update_cond_flags(lc3, lc3->reg[dr]);
}

void process_ldr(lc3_state* lc3, uint16_t instr)
{
  uint16_t dr = ENCODE_PARAM(instr, 9, 3);
  uint16_t base_r = ENCODE_PARAM(instr, 6, 3);
  uint16_t offset6 = ENCODE_PARAM(instr, 0, 6);

  lc3->reg[dr] = mem_read(lc3, lc3->reg[base_r] + sign_extend(offset6, 6));
  update_cond_flags(lc3, lc3->reg[dr]);
}

void process_lea(lc3_state* lc3, uint16_t instr)
{
  uint16_t dr = ENCODE_PARAM(instr, 9, 3);
  uint16_t pc_offset9 = ENCODE_PARAM(instr, 0, 9);

  lc3->reg[dr] = lc3->reg[R_PC] + sign_extend(pc_offset9, 9);
  update_cond_flags(lc3, lc3->reg[dr]);
}

void process_st(lc3_state* lc3, uint16_t instr)
{
  uint16_t sr = ENCODE_PARAM(instr, 9, 3);
  uint16_t pc_offset9 = ENCODE_PARAM(instr, 0, 9);
  uint16_t address = lc3->reg[R_PC] + sign_extend(pc_offset9, 9);

  mem_write(lc3, address, lc3->reg[sr]);
}

void process_sti(lc3_state* lc3, uint16_t instr)
{
  uint16_t sr = ENCODE_PARAM(instr, 9, 3);
  uint16_t pc_offset9 = ENCODE_PARAM(instr, 0, 9);
  uint16_t address = mem_read(lc3, mem_read(lc3, lc3->reg[R_PC] + sign_extend(pc_offset9, 9)));

  mem_write(lc3, address, lc3->reg[sr]);
}

void process_str(lc3_state* lc3, uint16_t instr)
{
  uint16_t sr = ENCODE_PARAM(instr, 9, 3);
  uint16_t base_r = ENCODE_PARAM(instr, 6, 3);
  uint16_t offset6 = ENCODE_PARAM(instr, 0, 6);
  uint16_t address = lc3->reg[base_r] + sign_extend(offset6, 6);

  mem_write(lc3, address, lc3->reg[sr]);
}

void handle_trap_getc(lc3_state* lc3)
{
  lc3->reg[R_R0] = (uint16_t)getchar();
}

void handle_trap_out(lc3_state* lc3)
{
  putc((char)lc3->reg[R_R0], stdout);
  fflush(stdout);
}

void handle_trap_puts(lc3_state* lc3)
{
  uint16_t* c = lc3->memory + lc3->reg[R_R0];
  while (*c)
  {
    putc((char)*c, stdout); 
    ++c;
  }
  fflush(stdout);
}

void handle_trap_in(lc3_state* lc3)
{
  printf("Enter a character: ");
  char c = getchar();
  putc(c, stdout);
  lc3->reg[R_R0] = (uint16_t)c;
}

void handle_trap_putsp(lc3_state* lc3)
{
  uint16_t* c = lc3->memory + lc3->reg[R_R0];
  while (*c)
  {
      char char1 = (*c) & 0xFF;
      putc(char1, stdout);
      char char2 = (*c) >> 8;
      if (char2) putc(char2, stdout);
      ++c;
  }
  fflush(stdout);
}

void handle_trap_halt(lc3_state* lc3)
{
  puts("HALT");
  fflush(stdout);
  lc3->running = false;
}

void process_trap(lc3_state* lc3, uint16_t instr)
{
  uint16_t trapvect8 = instr & 0xFF;
  switch (trapvect8)
  {
    case TRAP_GETC:
      handle_trap_getc(lc3);
      break;
    case TRAP_OUT:
      handle_trap_out(lc3);
      break;
    case TRAP_PUTS:
      handle_trap_puts(lc3);
      break;
    case TRAP_IN:
      handle_trap_in(lc3);
      break;
    case TRAP_PUTSP:
      handle_trap_putsp(lc3);
      break;
    case TRAP_HALT:
      handle_trap_halt(lc3);
      break;
  }
}

void process_badopcode(lc3_state* lc3, uint16_t instr)
{
  abort();
}

void process_instr(lc3_state* lc3, uint16_t instr)
{
  uint16_t op = instr >> 12;
  
  switch (op)
  {
    case OP_ADD:
      process_add(lc3, instr);
      break;
    case OP_AND:
      process_and(lc3, instr);
      break;
    case OP_NOT:
      process_not(lc3, instr);
      break;
    case OP_BR:
      process_br(lc3, instr);
      break;
    case OP_JMP:
      process_jmp(lc3, instr);
      break;
    case OP_JSR:
      process_jsr(lc3, instr);
      break;
    case OP_LD:
      process_ld(lc3, instr);
      break;
    case OP_LDI:
      process_ldi(lc3, instr);
      break;
    case OP_LDR:
      process_ldr(lc3, instr);
      break;
    case OP_LEA:
      process_lea(lc3, instr);
      break;
    case OP_ST:
      process_st(lc3, instr);
      break;
    case OP_STI:
      process_sti(lc3, instr);
      break;
    case OP_STR:
      process_str(lc3, instr);
      break;
    case OP_TRAP:
      process_trap(lc3, instr);
      break;
    case OP_RES:
    case OP_RTI:
    default:
      process_badopcode(lc3, instr);
      break;
  }
}

struct termios original_tio;

void disable_input_buffering()
{
  tcgetattr(STDIN_FILENO, &original_tio);
  struct termios new_tio = original_tio;
  new_tio.c_lflag &= ~ICANON & ~ECHO;
  tcsetattr(STDIN_FILENO, TCSANOW, &new_tio);
}

void restore_input_buffering()
{
  tcsetattr(STDIN_FILENO, TCSANOW, &original_tio);
}

void handle_interrupt(int signal)
{
  restore_input_buffering();
  printf("\n");
  exit(-2);
}

void load_program_to_memory(lc3_state* lc3, uint16_t origin, uint16_t* program_image, uint16_t instruction_count)
{
  for(int i = 0; i < instruction_count; i++)
  {
    lc3->memory[origin+i] = program_image[i];
  }
}

void run_lc3(lc3_ptr lc3arg, uint16_t origin, uint16_t* program_image, uint16_t instruction_count)
{
  lc3_state* lc3 = (lc3_state*) lc3arg;
  load_program_to_memory(lc3, origin, program_image, instruction_count);

  signal(SIGINT, handle_interrupt);
  disable_input_buffering();

  const uint16_t PC_START = 0x3000;
  lc3->reg[R_PC] = PC_START;

  lc3->running = true;
  while (lc3->running)
  {
    uint16_t instr = mem_read(lc3, lc3->reg[R_PC]++);
    process_instr(lc3, instr);
  }

  restore_input_buffering();
}
