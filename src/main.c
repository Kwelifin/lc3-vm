#include <stdlib.h>
#include <stdio.h>
#include "lc3.h"

uint16_t swap16(uint16_t x)
{
  return (x << 8) | (x >> 8);
}

void free_program_image(uint16_t* program_image)
{
  free(program_image);
}

void read_program_image_file(FILE* file, uint16_t* origin, uint16_t** program_image, uint16_t* instruction_count)
{
  fread(origin, sizeof(uint16_t), 1, file);
  *origin = swap16(*origin);

  uint16_t max_read = UINT16_MAX - *origin;
  *program_image = (uint16_t*)malloc(sizeof(uint16_t) * max_read);
  size_t read = fread(*program_image, sizeof(uint16_t), max_read, file);
  *instruction_count = max_read;

  uint16_t* p = *program_image;

  while ( read-- > 0)
  {
    *p = swap16(*p);
    ++p;
  }
}

int read_program_image(const char* image_path, uint16_t* origin, uint16_t** program_image, uint16_t* instruction_count)
{
  FILE* file = fopen(image_path, "rb");
  if (!file) { return 0; };
  read_program_image_file(file, origin, program_image, instruction_count);
  fclose(file);
  return 1;
}

int main(int argc, const char* argv[])
{
  if (argc < 2)
  {
    printf("argument missing, lc3 [program_path] ... \n");
    exit(2);
  }

  uint16_t origin;
  uint16_t* program_image;
  uint16_t instruction_count;
  for (int j = 1; j < argc; ++j)
  {
    if (!read_program_image(argv[j], &origin, &program_image, &instruction_count))
    {
      printf("failed to load program image: %s\n", argv[j]);
    }
  }

  lc3_ptr lc3 = new_lc3();
  run_lc3(lc3, origin, program_image, instruction_count);
  destroy_lc3(lc3);
  free_program_image(program_image);
  return 0;
}
