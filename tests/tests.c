//#include <stdio.h>
#include "minunit.h"
#include <stdlib.h>
#include "lc3internal.h"
#include "lc3.h"

MU_TEST(test_add_register) {
  lc3_ptr lc3_external = new_lc3();
  lc3_state* lc3 = (lc3_state*) lc3_external;

  lc3->reg[R_R0] = 0;
  lc3->reg[R_R1] = 7;
  lc3->reg[R_R2] = -4;

  uint16_t instr = 0;
  instr |= OP_ADD << 12; // OPCODE
  instr |= R_R0 << 9;    // DR
  instr |= R_R1 << 6;    // SR1
  instr |= R_R2;         // SR2

  process_instr(lc3, instr);

  mu_check(lc3->reg[R_R0] == 3);
  mu_check(lc3->reg[R_COND] & COND_FL_POS);

  destroy_lc3(lc3_external);
}

MU_TEST(test_add_literal) {
  lc3_ptr lc3_external = new_lc3();
  lc3_state* lc3 = (lc3_state*) lc3_external;

  lc3->reg[R_R0] = 0;
  lc3->reg[R_R1] = 7;
  uint8_t literal = (-9) & ((1 << 5)-1);

  uint16_t instr = 0;
  instr |= OP_ADD << 12; // OPCODE
  instr |= R_R0 << 9;    // DR
  instr |= R_R1 << 6;    // SR1
  instr |= 1 << 5;       // literal flag
  instr |= literal;      // imm5

  process_instr(lc3, instr);

  mu_check(lc3->reg[R_R0] == (uint16_t)-2);
  mu_check(lc3->reg[R_COND] & COND_FL_NEG);

  destroy_lc3(lc3_external);
}

MU_TEST(test_and_register) {
  lc3_ptr lc3_external = new_lc3();
  lc3_state* lc3 = (lc3_state*) lc3_external;

  lc3->reg[R_R0] = 0;
  lc3->reg[R_R1] = 7;
  lc3->reg[R_R2] = 14;
  
  uint16_t instr = 0;
  instr |= OP_AND << 12; // OPCODE
  instr |= R_R0 << 9;    // DR
  instr |= R_R1 << 6;    // SR1
  instr |= R_R2;         // SR2

  process_instr(lc3, instr);

  mu_check(lc3->reg[R_R0] == (uint16_t)6);
  mu_check(lc3->reg[R_COND] & COND_FL_POS);

  destroy_lc3(lc3_external);
}

MU_TEST(test_and_literal) {
  lc3_ptr lc3_external = new_lc3();
  lc3_state* lc3 = (lc3_state*) lc3_external;

  lc3->reg[R_R0] = 0;
  lc3->reg[R_R1] = 7;
  uint16_t literal = -2 & ((1<<5)-1);
  
  uint16_t instr = 0;
  instr |= OP_AND << 12; // OPCODE
  instr |= R_R0 << 9;    // DR
  instr |= R_R1 << 6;    // SR1
  instr |= 1 << 5;       // literal flag
  instr |= literal;      // SR2

  process_instr(lc3, instr);

  mu_check(lc3->reg[R_R0] == (uint16_t)6);
  mu_check(lc3->reg[R_COND] & COND_FL_POS);

  destroy_lc3(lc3_external);
}

MU_TEST(test_br_zp) {
  lc3_ptr lc3_external = new_lc3();
  lc3_state* lc3 = (lc3_state*) lc3_external;

  lc3->reg[R_PC] = 0x300F;
  lc3->reg[R_COND] = COND_FL_ZRO;
  
  uint16_t instr = 0;
  instr |= OP_BR << 12; // OPCODE
  instr |= 0b011 << 9;  // n = 0, z = 1, p = 1
  instr |= 0xFF;        // just FF offset

  process_instr(lc3, instr);

  mu_check(lc3->reg[R_PC] == (uint16_t)(0x300F+0xFF));

  destroy_lc3(lc3_external);
}

MU_TEST(test_br_n) {
  lc3_ptr lc3_external = new_lc3();
  lc3_state* lc3 = (lc3_state*) lc3_external;

  lc3->reg[R_PC] = 0x300F;
  lc3->reg[R_COND] = COND_FL_NEG;

  uint16_t instr = 0;
  instr |= OP_BR << 12; // OPCODE
  instr |= 0b011 << 9;  // n = 0, z = 1, p = 1
  instr |= 0xFF;        // just FF offset

  process_instr(lc3, instr);

  mu_check(lc3->reg[R_PC] == (uint16_t)(0x300F));
destroy_lc3(lc3_external);
}

MU_TEST(test_ldi) {
  lc3_ptr lc3_external = new_lc3();
  lc3_state* lc3 = (lc3_state*) lc3_external;

  lc3->reg[R_PC] = 0x300F;
  lc3->reg[R_R2] = 0x00F1;
  lc3->memory[0x300F + 0xAA] = 0x3FFA;
  lc3->memory[0x3FFA] = 0x0FFF;

  uint16_t instr = 0;
  instr |= OP_LDI << 12; // OPCODE
  instr |= R_R2 << 9;    // DR
  instr |= 0xAA;         // just FF offset

  process_instr(lc3, instr);

  mu_check(lc3->reg[R_R2] == (uint16_t)(0x0FFF));
  mu_check(lc3->reg[R_COND] & COND_FL_POS);

  destroy_lc3(lc3_external);
}

MU_TEST_SUITE(test_suite) {
  MU_RUN_TEST(test_add_register);
  MU_RUN_TEST(test_add_literal);
  MU_RUN_TEST(test_and_register);
  MU_RUN_TEST(test_and_literal);
  MU_RUN_TEST(test_br_zp);
  MU_RUN_TEST(test_br_n);
  MU_RUN_TEST(test_ldi);
}

int main(int argc, char *argv[]) {
  MU_RUN_SUITE(test_suite);
  MU_REPORT();
  return MU_EXIT_CODE;
}
