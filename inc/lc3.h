#ifndef LC3_CORE_H
#define LC3_CORE_H
#include <stdint.h>

typedef struct lc3state* lc3_ptr;

lc3_ptr new_lc3();
void destroy_lc3(lc3_ptr lc3);
void run_lc3(lc3_ptr lc3arg, uint16_t origin, uint16_t* program_image, uint16_t instruction_count);

#endif
