#ifndef LC3_INTERNAL_H
#define LC3_INTERNAL_H
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

enum registers
{
  R_R0 = 0, // R0-R7 register for general purposes
  R_R1,
  R_R2,
  R_R3,
  R_R4,
  R_R5,
  R_R6,
  R_R7,
  R_PC, // program counter (instruction pointer analog)
  R_COND, // conditional register, stored result of compare operation
  R_COUNT, // register count marker
};

enum opcodes
{
  OP_BR   = 0b0000, // branch
  OP_ADD  = 0b0001, // add
  OP_LD   = 0b0010, // load
  OP_ST   = 0b0011, // store
  OP_JSR  = 0b0100, // jump register
  OP_AND  = 0b0101, // bitwise add
  OP_LDR  = 0b0110, // load register
  OP_STR  = 0b0111, // store register
  OP_RTI  = 0b1000, // unused
  OP_NOT  = 0b1001, // bitwise not
  OP_LDI  = 0b1010, // load indirect
  OP_STI  = 0b1011, // store indirect
  OP_JMP  = 0b1100, // jump
  OP_RES  = 0b1101, // reserved (unused)
  OP_LEA  = 0b1110, // load effective address
  OP_TRAP = 0b1111, // execute trap
};

enum condition_flags
{
  COND_FL_POS = 0b001, // P - positive
  COND_FL_ZRO = 0b010, // Z - zero
  COND_FL_NEG = 0b100, // N - negative
};

enum traps
{
  TRAP_GETC  = 0x20,  // get character from keyboard, not echoed onto the terminal
  TRAP_OUT   = 0x21,  // output a character
  TRAP_PUTS  = 0x22,  // output a word string
  TRAP_IN    = 0x23,  // get character from keyboard, echoed onto the terminal
  TRAP_PUTSP = 0x24,  // output a byte string
  TRAP_HALT  = 0x25   // halt the program
};

enum memory_mapped_registers
{
  MR_KBSR = 0xFE00, // keyboard status
  MR_KBDR = 0xFE02, // keyboard data
};

typedef struct {
  uint16_t reg[R_COUNT];
  uint16_t memory[UINT16_MAX];
  bool running;
} lc3_state;

void process_instr(lc3_state* lc3, uint16_t instr);

#endif
